# config valid only for Capistrano 3.1
lock '3.2.1'

set :application, 'classicimportautoservice.com'
set :repo_url, 'git@bitbucket.org:noelement/classicimportautoservice.com.git'


set :branch, :master

set :deploy_to, "/var/www/classicimportautoservice.com"
set :log_level, :debug
set :keep_releases, 3

# Default value for :pty is false
set :pty, true

# Default value for :linked_files is []
# set :linked_files, %w{config/database.yml}

# Default value for linked_dirs is []

set :linked_files, %w{.env}
set :linked_dirs, %w{web/app/uploads}
set :theme_dir, 'app/themes/centric-pro'
 

namespace :taldeploy do
    task :wp_permissions do
      on roles(:app) do
          sudo "chown -R  :www-data #{release_path}/web"
      end
    end
end

before 'deploy:finished', 'taldeploy:wp_permissions'

namespace :composer do
  task :install do
    on roles(:all) do
      within release_path do
        sudo 'composer install --no-dev --prefer-dist --no-scripts --quiet --optimize-autoloader'
      end
    end
 end
end

before 'deploy:updated', 'composer:install'


namespace :deploy do
  desc 'Restart application'
    task :restart do
      on roles(:app) do
      # Your restart mechanism here, for example:
        sudo '/etc/init.d/nginx', 'reload'
      end
    end
end

# The above restart task is not run by default
# Uncomment the following line to run it on deploys if needed
after 'deploy:finished', 'deploy:restart'
